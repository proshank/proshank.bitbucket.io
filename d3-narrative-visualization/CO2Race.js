var co2Data
function init() {

    d3.csv('https://bitbucket.org/proshank/proshank.bitbucket.io/raw/fe0d96de6e0eacd51d5ce0f3b0c2592fa5a0481c/d3-narrative-visualization/historical_emissions_total_pivot.csv')
        .then(function (data) {
            co2Data = data;
            visualize();
        });
}

function visualize() {
    d3.select("#submitVisualization").attr('disabled', true);

    startYear = document.getElementById('sYear').value;
    endYear = document.getElementById('eYear').value;

    year = startYear;
    tickDuration = 333;
    top_n = 11;
    height = 600;
    width = document.getElementsByClassName("visualization")[0].clientWidth;

    halo = function (text, strokeWidth) {
        text.select(function () { return this.parentNode.insertBefore(this.cloneNode(true), this); })
            .style("fill", '#ffffff')
            .style("stroke", '#ffffff')
            .style("stroke-width", strokeWidth)
            .style("stroke-linejoin", 'round')
            .style("opacity", 1);
    };

    d3.selectAll(".visualization > *").remove();
    d3.selectAll(".importantEvents > *").remove();
    const svg = d3.select('.visualization').append('svg').attr('width', width).attr('height', height);

    const margin = {
        top: 80,
        right: 20,
        bottom: 5,
        left: 20
    };

    let barPadding = (height - (margin.bottom + margin.top)) / (top_n * 5);

    let title = svg.append('text')
        .attr("class", "title")
        .attr("x", margin.left)
        .attr("y", 24)
        .html('Countries with largest CO2 emissions from 1850 to 2016');

    let subTitle = svg.append('text')
        .attr("class", "subTitle")
        .attr("x", margin.left)
        .attr("y", 55)
        .html('Annual CO2 emissions(Million Tonnes)');

    let caption = svg.append('text')
        .attr("class", 'caption')
        .attr("x", width)
        .attr("y", height - 5)
        .style("text-anchor", "end")


    co2Data.forEach(d => {
        d.value = +d.value,
            d.lastValue = +d.lastValue,
            d.value = isNaN(d.value) ? 0 : d.value,
            d.year = +d.year,
            d.colour = d3.hsl(Math.random() * 360, 0.75, 0.75)
    });

    let yearSlice = co2Data.filter(d => d.year == year && !isNaN(d.value))
        .sort((a, b) => b.value - a.value)
        .slice(0, top_n);

    yearSlice.forEach((d, i) => d.rank = i);

    let x = d3.scaleLog()
        .domain([10, d3.max(yearSlice, d => d.value)])
        .range([margin.left, width - margin.right - 65]);

    let y = d3.scaleLinear()
        .domain([top_n, 0])
        .range([height - margin.bottom, margin.top]);

    let xAxis = d3.axisTop()
        .scale(x)
        .ticks(width > 500 ? 5 : 2)
        .tickSize(-(height - margin.top - margin.bottom))
        .tickFormat(d => d3.format(',.0s')(d));

    svg.append('g')
        .attr("class", "axis xAxis")
        .attr("transform", "translate(0," + margin.top + ")")
        .call(xAxis)
        .selectAll('.tick line')
        .classed('origin', d => d == 0);

    svg.selectAll('rect.bar')
        .data(yearSlice, d => d.name)
        .enter()
        .append('rect')
        .attr("class", "bar")
        .attr("x", x(10) + 1)
        .attr("width", d => x(d.value) - x(10) - 1)
        .attr("y", d => y(d.rank) + 5)
        .attr("height", y(1) - y(0) - barPadding)
        .style("fill", d => d.colour);

    svg.selectAll('text.label')
        .data(yearSlice, d => d.name)
        .enter()
        .append('text')
        .attr("class", "label")
        .attr("x", d => x(d.value) - 8)
        .attr("y", d => y(d.rank) + 5 + ((y(1) - y(0)) / 2) + 1)
        .attr("text-anchor", "end")
        .html(d => d.name);

    svg.selectAll('text.valueLabel')
        .data(yearSlice, d => d.name)
        .enter()
        .append('text')
        .attr("class", "valueLabel")
        .attr("x", d => x(d.value) + 5)
        .attr("y", d => y(d.rank) + 5 + ((y(1) - y(0)) / 2) + 1)
        .text(d => d3.format(',.0f')(d.lastValue));

    let yearText = svg.append('text')
        .attr("class", "yearText")
        .attr("x", width - margin.right)
        .attr("y", height - 2)
        .style("text-anchor", "end")
        .html(~~year)
        .call(halo, 10);

    let yearTextPercent = d3.selectAll(".year").html(~~year);
    d3.select(".sYearQuestion").html(~~year);
    let carbonBudegt = (d3.max(yearSlice, d => d.value));
    let yearPercent = d3.select(".totalCO2Spent").html(d3.format(',')(carbonBudegt));

    let importatEventsText = d3.select(".importantEvents");
    let lastTopCO2EmmitingCountry = "";
    let lastTopCo2year = 0;
    let topCO2EmmitingCountry = yearSlice.filter(data => data.value == d3.max(yearSlice, d => d.name !== 'World' ? d.value : 0))[0].name;
    if (lastTopCO2EmmitingCountry != topCO2EmmitingCountry) {
        if (year - lastTopCo2year > 5) { importatEventsText.append().html("<p>Year " + year + ", " + topCO2EmmitingCountry + " emerged as highest CO2 emitting country.</p>"); }
        lastTopCO2EmmitingCountry = topCO2EmmitingCountry;
        lastTopCo2year = year;
    }
    let ticker = d3.interval(e => {

        yearSlice = co2Data.filter(d => d.year == year && !isNaN(d.value))
            .sort((a, b) => b.value - a.value)
            .slice(0, top_n);

        yearSlice.forEach((d, i) => d.rank = i);

        x.domain([10, d3.max(yearSlice, d => d.value)]);

        svg.select('.xAxis')
            .transition()
            .duration(tickDuration)
            .ease(d3.easeLinear)
            .call(xAxis);

        let bars = svg.selectAll('.bar').data(yearSlice, d => d.name);

        bars
            .enter()
            .append('rect')
            .attr("class", d => "bar ${d.name.replace(/\s/g, '_')}")
            .attr("x", x(10) + 1)
            .attr("width", d => x(d.value) - x(10) - 1)
            .attr("y", d => y(top_n + 1) + 5)
            .attr("height", y(1) - y(0) - barPadding)
            .style("fill", d => d.colour)
            .transition()
            .duration(tickDuration)
            .ease(d3.easeLinear)
            .attr("y", d => y(d.rank) + 5);

        bars
            .transition()
            .duration(tickDuration)
            .ease(d3.easeLinear)
            .attr("width", d => x(d.value) - x(10) - 1)
            .attr("y", d => y(d.rank) + 5);

        bars
            .exit()
            .transition()
            .duration(tickDuration)
            .ease(d3.easeLinear)
            .attr("width", d => x(d.value) - x(10) - 1)
            .attr("y", d => y(top_n + 1) + 5)
            .remove();

        let labels = svg.selectAll('.label').data(yearSlice, d => d.name);

        labels
            .enter()
            .append('text')
            .attr("class", 'label')
            .attr("x", d => x(d.value) - 8)
            .attr("y", d => y(top_n + 1) + 5 + ((y(1) - y(0)) / 2))
            .attr("text-anchor", 'end')
            .html(d => d.name)
            .transition()
            .duration(tickDuration)
            .ease(d3.easeLinear)
            .attr("y", d => y(d.rank) + 5 + ((y(1) - y(0)) / 2) + 1);

        labels
            .transition()
            .duration(tickDuration)
            .ease(d3.easeLinear)
            .attr("x", d => x(d.value) - 8)
            .attr("y", d => y(d.rank) + 5 + ((y(1) - y(0)) / 2) + 1);

        labels
            .exit()
            .transition()
            .duration(tickDuration)
            .ease(d3.easeLinear)
            .attr("x", d => x(d.value) - 8)
            .attr("y", d => y(top_n + 1) + 5)
            .remove();

        let valueLabels = svg.selectAll('.valueLabel').data(yearSlice, d => d.name);

        valueLabels
            .enter()
            .append('text')
            .attr("class", 'valueLabel')
            .attr("x", d => x(d.value) + 5)
            .attr("y", d => y(top_n + 1) + 5)
            .text(d => d3.format(',.0f')(d.lastValue))
            .transition()
            .duration(tickDuration)
            .ease(d3.easeLinear)
            .attr("y", d => y(d.rank) + 5 + ((y(1) - y(0)) / 2) + 1);

        valueLabels
            .transition()
            .duration(tickDuration)
            .ease(d3.easeLinear)
            .attr("x", d => x(d.value) + 5)
            .attr("y", d => y(d.rank) + 5 + ((y(1) - y(0)) / 2) + 1)
            .tween("text", function (d) {
                let i = d3.interpolateRound(d.lastValue, d.value);
                return function (t) {
                    this.textContent = d3.format(',')(i(t));
                };
            });

        valueLabels
            .exit()
            .transition()
            .duration(tickDuration)
            .ease(d3.easeLinear)
            .attr("x", d => x(d.value) + 5)
            .attr("y", d => y(top_n + 1) + 5)
            .remove();

        yearText.html(~~year);
        yearTextPercent.html(~~year);
        carbonBudegt = carbonBudegt + (d3.max(yearSlice, d => d.value));
        yearPercent.html(d3.format(',')(carbonBudegt))

        topCO2EmmitingCountry = yearSlice.filter(data => data.value == d3.max(yearSlice, d => d.name !== 'World' ? d.value : 0))[0].name;
        if (lastTopCO2EmmitingCountry != topCO2EmmitingCountry) {
            if (year - lastTopCo2year > 5) { importatEventsText.append().html("<p>Year " + year + ", " + topCO2EmmitingCountry + " emerged as highest CO2 emitting country.</p>"); }
            lastTopCO2EmmitingCountry = topCO2EmmitingCountry;
            lastTopCo2year = year;
        }

        if (year == endYear) {
            ticker.stop();
            d3.select("#submitVisualization").attr('disabled', null);
            d3.select("#exploreMore").attr('disabled', null).style("background-color", "#1c87c9").style("color", "white");
        }
        year = (+year) + 1;
    }, tickDuration);
    return false;
}