//main JS file
var width = 800;
var height = 600;
var minValue, maxValue, geoData, totalData;

d3.csv('https://bitbucket.org/proshank/proshank.bitbucket.io/raw/645c169db7a9a80736c21c8668638cd352ead91b/d3-narrative-visualization/historical_emissions.csv')
	.then(function (countryData) {
		d3.json("https://unpkg.com/world-atlas@1/world/110m.json").then(function (topoData) {
			totalData = countryData;
			minValue = d3.min(countryData, d => d.year);
			maxValue = d3.max(countryData, d => d.year);
			//d3.select("#year").text(maxValue);
			geoData = topojson.feature(topoData, topoData.objects.countries).features;
			var yearData = countryData.filter(d => d.year === maxValue);

			var map = d3.select("#map")
				.selectAll('svg')
				.attr("width", width)
				.attr("height", height);

			var bar = d3.select("#barchart")
				.selectAll('svg')
				.attr('width', 500)
				.attr('height', 300)

			drawMap(yearData);
		});
	});
