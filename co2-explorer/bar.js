//bar JS file
var drawBar = function (countryData) {

	countryData = countryData.sort((a, b) => a.year - b.year);

	var barSvg = d3.select("#barchart")
		.select("svg");

	var barChartWidth = barSvg.attr("width");

	var barChartHeight = barSvg.attr("height");

	var barPadding = 2, xPadding = 60, yPadding = 30;

	var barWidth = (barChartWidth - 2 * xPadding) / countryData.length;

	if (countryData.length > 0) {
		barSvg.selectAll('.heading').remove();

		barSvg.append('text')
			.classed('heading', true)
			.attr('x', barChartWidth / 2)
			.attr('y', yPadding / 1.5)
			.attr('text-anchor', 'middle')
			.attr('font-size', '1.5em')
			.text(countryData[0].name);

		var bars = d3.select("#barchart")
			.select("svg")
			.selectAll(".bar")
			.data(countryData);

		var xScale = d3.scaleLinear()
			.domain(d3.extent(countryData, d => +d.year))
			.range([xPadding, barChartWidth - xPadding]);

		var yScale = d3.scaleLinear()
			.domain(d3.extent(countryData, d => +d.value))
			.range([barChartHeight - yPadding, yPadding]);

		var xAxis = d3.axisBottom(xScale);

		var yAxis = d3.axisLeft(yScale);

		barSvg.selectAll('.axes').remove();

		barSvg.append('g')
			.attr('transform', 'translate(0,' + (barChartHeight - yPadding) + ')')
			.call(xAxis)
			.classed('axes', true);

		// text label for the x axis
		barSvg.append("text")
			.attr("transform",
				"translate(" + (+barChartWidth / 2) + "," + (+barChartHeight) + ")")
			.style("text-anchor", "middle")
			.text("Year");

		barSvg.append('g')
			.attr('transform', 'translate(' + xPadding + ',0)')
			.call(yAxis)
			.classed('axes', true);

		// text label for the y axis
		barSvg.append("text")
			.attr("transform", "rotate(-90)")
			.attr("y", 0)
			.attr("x", 0 - (barChartHeight / 2))
			.attr("dy", "1em")
			.style("text-anchor", "middle")
			.text("CO2 Emissions (Millions Tons)");

		bars.exit().remove();

		bars.enter()
			.append('rect')
			.classed('bar', true)
			.merge(bars)
			.attr('x', d => xScale(d.year))
			.transition()
			.ease(d3.easeLinear)
			.attr('y', d => yScale(d.value))
			.attr('width', barWidth - barPadding)
			.attr('height', d => barChartHeight - yScale(d.value) - yPadding);
	} else {
		d3.select("#barchart")
			.selectAll("svg > *").remove();
		barSvg.append('text')
			.classed('heading', true)
			.attr('x', barChartWidth / 2)
			.attr('y', yPadding / 1.5)
			.attr('text-anchor', 'middle')
			.attr('font-size', '1.5em')
			.text('NO DATA AVAILABLE FOR THIS COUNTRY');
	}
}