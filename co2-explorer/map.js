//maps JS file
var drawMap = function (yearData) {

	d3.selectAll(".bar").remove();

	var projection = d3.geoMercator()
		.scale(120)
		.translate([width / 2, height / 2]);

	var path = d3.geoPath()
		.projection(projection);

	var color = d3.scaleLog()
		.domain([0.01, 0.1, 1, 10, 100, 200, 500, 1000, 2000, 10000])
		.range(["rgb(247,251,255)", "rgb(222,235,247)", "rgb(198,219,239)", "rgb(158,202,225)", "rgb(107,174,214)", "rgb(66,146,198)", "rgb(33,113,181)", "rgb(8,81,156)", "rgb(8,48,107)", "rgb(3,19,43)"]);

	var atlas = d3.select("#map")
		.select('svg')
		.selectAll('.map')
		.data(geoData);


	atlas.style("stroke", "none")
		.attr("fill", function(d) { 
			cData = yearData.filter(c=> +c.countryCode == +d.id)[0];
			return cData ? color(+cData.value) : '#ccc';
	});

	var prevSelected;

	atlas.enter()
		.append("path")
		.classed("map", true)
		.on('click', function (d) {
			var yearlyCountryData = totalData.filter(c => +c.countryCode == +d.id);
			d3.select(this).style('stroke', 'black').style('stroke-width', 1);
			if (prevSelected && prevSelected != this) d3.select(prevSelected).style('stroke', 'none');
			if (prevSelected != this) drawBar(yearlyCountryData);
			prevSelected = this;
		})
		.attr("d", path)
		.attr("fill", function(d) { 
			cData = yearData.filter(c=> +c.countryCode == +d.id)[0];
			return cData ? color(+cData.value) : '#ccc';
		});
}